﻿// Searchbar 
function showBox() {
    var box = $("#box");
    if (box.is(":visible")) {
        box.hide();
    }
    else {
        box.show();
    }

}

getFooterImg();
getSponserImg();
getSlideImg();

// pagenumber
pag(1);

// Click function
$(".pag").click(function (e) {
    e.preventDefault();
    var pageNumber = $(this).html(); // creates variable with value of item clicked
    $("#pageNumber").html(pageNumber); // sets the visible active page to the number contained in the pagenumber varible
    pag(pageNumber); // call the page shifting function 

})

// 
function pag(number) {
    $(".post").hide(); // hides all posts
    var start = 3 * (number - 1); // store the start index in a variable
    $(".post").slice(start, start +3 ).show(); // shows the posts from start index with an interval of 3 
}
    // Footer images
function getFooterImg() {
    var folder = "http://localhost/Img/footer/";

    $.ajax({
        url: folder,
        success: function (data) {
            $(data).find("a").attr("href", function (i, imgSti) {
                if (imgSti.match(/\.(jpe?g|png|gif)$/)) {
                    $(".footerImgs").append("<div class='col-lg-4 col-md-4 col-sm-4 col-xs-4'><img src='" + folder + imgSti + "'  class='footerImg'></div>");
                }
            });
        }
    });
}


// Sponsor images
function getSponserImg() {
    var folder = "http://localhost/Img/sponsorer/";

    $.ajax({
        url: folder,
        success: function (data) {
            $(data).find("a").attr("href", function (i, imgSti) {
                if (imgSti.match(/\.(jpe?g|png|gif)$/)) {
                    $(".sponsorContainer").append("<div class='col-lg-2 col-md-2 col-sm-3 col-xs-3'><img src='" + folder + imgSti + "'  class='sponsorImg'></div>");
                }
            });
        }
    });
}

    // Slideshow images
function getSlideImg() {
    var folder = "http://localhost/Img/slider/";

    $.ajax({
        url: folder,
        success: function (data) {
            $(data).find("a").attr("href", function (i, imgSti) {
                if (imgSti.match(/\.(jpe?g|png|gif)$/)) {
                    $(".carousel-inner").append("<div class='item'><img src='" + folder + imgSti + "'  class='sliderImg'></div>");
                }
            });
            $(".carousel-inner .item").first().addClass("active");
        }
    });
   
}
